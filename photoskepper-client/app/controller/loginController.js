app.controller('loginController',['$rootScope','$cookies','$scope','config','$location','authSvc',
function($rootScope,$cookies,$scope,config,$location,authSvc){
    this.authSvc =authSvc;
    $scope.wrongCred = false;
    $scope.successRegistration = false;
    $scope.registrationError = false;
    $scope.registrationErrorMessage = "";
    $scope.displayLogin = true;
    $scope.changeDisplay= function(value){
         $scope.displayLogin=value;
    }
    this.login = function(){
        var data = {
        email: $scope.email,
        password: $scope.password
        }
        this.authSvc.login(data).then(function successCallback(response) {
            $scope.wrongCred = false;
            $rootScope.isLogged= true;
            $rootScope.token=response.data;
            $cookies.put('token', response.data);
            $location.path('/home');
        }, function errorCallback(response) {
            $scope.wrongCred = true;
        });
    };

    this.logout = function(){
        this.authSvc.logout().then(function successCallback(response) {
            $scope.wrongCred = false;
            $rootScope.isLogged= false;
            $cookies.put('token', "");
            $location.path('/login');
       }, function errorCallback(response) {

     });
    }


    this.register = function(){
        var newUser = {
        email: $scope.newEmail,
        password: $scope.newPassword
        }
        this.authSvc.register(newUser).then(function successCallback(response) {
            $scope.successRegistration = true;
            $scope.registrationError = false;
            $location.path('/login');
        }, function errorCallback(response) {
            $scope.registrationError = true;
             $scope.successRegistration = false;
            if(response.data.errorCode==1200){
            $scope.registrationErrorMessage ="Hasło powinno zawierac od 8 do 20 znaków w tym jedną dużą i małą literę znak specjalny oraz cyfrę"
            } else if(response.data.errorCode==1100){
            $scope.registrationErrorMessage ="Istnieje użytkownik z podanym mailem"
            } else if(response.data.errorCode==1750){
            $scope.registrationErrorMessage ="Nie poprwany format email"
            } else{
            $scope.registrationErrorMessage ="Błąd podczas rejestracji"
            }
            console.log(response);
        });
    };

}
])