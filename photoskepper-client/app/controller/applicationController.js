app.controller('applicationController',['$rootScope','$location','$window','$scope','fileSvc','$cookies','authSvc','config',
  function($rootScope,$location,$window,$scope,fileSvc,$cookies,authSvc,config){
      $scope.config = config;
      $scope.fileSvc = fileSvc;
      $scope.image = '';
      var photosResponse = fileSvc.getPhotos();
      $scope.authSvc = authSvc;
      $scope.showError = false
        $scope.showModal = false;
        $scope.toggleModal = function(){
        $scope.showPhoto = !$scope.showPhoto;
    };
    photosResponse.then(function successCallback(response) {
       $scope.photos=response.data;
       }, function errorCallback(response) {
        if(response.data.status==401){
           $location.path('/login');
         }
       });

     $scope.uploadFile = function(file){
              $scope.fileSvc.uploadFile(file.files[0]).then(function successCallback(response) {
               $window.location.reload();
             }, function errorCallback(response) {
                if(response.data.status==401){
                         $location.path('/login');
                }else{
                $scope.showError= true;
                }
             });
     }

     $scope.logout = function(){
      $scope.authSvc.logout().then(function successCallback(response) {
         $rootScope.isLogged= false;
         $cookies.put('token', "");
         $location.path('/login');
     }, function errorCallback(response) {
            if(response.data.status==401){
              $location.path('/login');
             }
     });
     }

       $scope.closePopup = function(){
       $scope.showPhoto= false;
       $scope.image='';
       }


     $scope.getPhoto = function(photoID){
        $scope.fileSvc.getPhotoById(photoID.photo.id).then(function successCallback(response) {
        $scope.showPhoto = true;
        $scope.image = response.data;
        $scope.toggleModal(test);
         }, function errorCallback(response) {
          if(response.data.status==401){
             $location.path('/login');
        }else{
            $scope.showError= true;
          }
       });
     }

  }
  ])