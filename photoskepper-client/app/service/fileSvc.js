app.service('fileSvc',['$http','config','$cookies',
function($http,config,$cookies){
     this.$http=$http;

     this.uploadFile = function(file){
      var formData = new FormData();
      formData.append('file', file, file.name);
      return this.$http({
                             method: 'POST',
                             url: config.SERVER_URL+config.API_URL+'/file',
                             data: formData,
                             headers: {
                                 "auth_header": $cookies.get('token'),
                                 "Content-Type": undefined
                             }
                         });
      }


      this.getPhotos = function(){
      return this.$http.get(config.SERVER_URL+config.API_URL+'/file');

      }
      this.getPhotoById = function(id){
      return this.$http.get(config.SERVER_URL+config.API_URL+'/file/'+id);

      }
}
])