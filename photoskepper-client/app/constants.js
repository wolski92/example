app.constant('config', {
  APPLICATION_NAME:       'PhotoKeeper',
  VERSION:                '0.0.1',
  SERVER_URL:             'http://localhost:8080',
  API_URL:                '/api',
  AUTH_URL:               '/auth',
  LOGOUT_URL:             '/auth/logout',
});