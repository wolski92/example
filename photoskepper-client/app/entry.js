var app = angular.module('app', ['ngRoute','ngCookies','ngResource'
]);

app.factory('httpRequestInterceptor',['$cookies',
    function($cookies){
       return {
        request: function(config) {
        if($cookies.get('token')){
          config.headers['auth_header'] = $cookies.get('token');
        }
       return config;
       }
      };
     }]);

app.config(['$injector','$httpProvider',
 function($injector,$httpProvider){
    var config = $injector.get('config');
    $httpProvider.interceptors.push('httpRequestInterceptor');


}])


app.run(['$rootScope', '$log', 'config',
  function ($rootScope, $log, config) {

      $log.info('---------------------------------------------------------------------------');
      $log.info('Application "' + config.APPLICATION_NAME + '" initialized.');
      $log.info('---------------------------------------------------------------------------');
      $log.info('Client Version: ' + config.VERSION);
      // $log.info(' AUTH Endpoint: ' + config.AUTH_URL);
      $log.info('  Connected to server: ' + config.SERVER_URL);
      $log.info('---------------------------------------------------------------------------');
  }
]);