app.config(['$routeProvider',
  function ($routeProvider) {

  $routeProvider

              .when('/login', {
                  controller: 'loginController',
                  templateUrl: 'view/login.html',
                  controllerAs: 'loginController'
              })
               .when('/home', {
                  controller: 'applicationController',
                  templateUrl: 'view/app.html',
                  controllerAs: 'applicationController'
              })
              .when('/register', {
                  controller: 'registerController',
                  templateUrl: 'view/register.html',
                  controllerAs: 'registerController'
              })
              .when('/error',{
                    templateUrl: 'view/error.html',

              })
              .otherwise({ redirectTo: '/login' });

}])

