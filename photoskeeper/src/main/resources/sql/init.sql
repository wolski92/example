--
-- PostgreSQL database dump
--

-- Dumped from database version 10.1
-- Dumped by pg_dump version 10.1

-- Started on 2018-01-21 22:01:57

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 10 (class 2615 OID 16395)
-- Name: photoskeeper; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA photoskeeper;


ALTER SCHEMA photoskeeper OWNER TO postgres;

SET search_path = photoskeeper, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 200 (class 1259 OID 16447)
-- Name: photos; Type: TABLE; Schema: photoskeeper; Owner: postgres
--

CREATE TABLE photos (
    id character varying(255) NOT NULL,
    user_id character varying(255) NOT NULL,
    file_name character varying(255),
    original_file bytea NOT NULL,
    thumbnail bytea,
    creation_date timestamp without time zone NOT NULL
);


ALTER TABLE photos OWNER TO postgres;

--
-- TOC entry 201 (class 1259 OID 16460)
-- Name: tokens; Type: TABLE; Schema: photoskeeper; Owner: postgres
--

CREATE TABLE tokens (
    id character varying(255) NOT NULL,
    id_user character varying(255) NOT NULL,
    creation_date timestamp without time zone NOT NULL,
    expiration_date timestamp without time zone NOT NULL
);


ALTER TABLE tokens OWNER TO postgres;

--
-- TOC entry 199 (class 1259 OID 16404)
-- Name: user; Type: TABLE; Schema: photoskeeper; Owner: postgres
--

CREATE TABLE "user" (
    id character varying(255) NOT NULL,
    email character varying(255) NOT NULL,
    password character varying(255) NOT NULL
);


ALTER TABLE "user" OWNER TO postgres;

--
-- TOC entry 2697 (class 2606 OID 16454)
-- Name: photos photos_pkey; Type: CONSTRAINT; Schema: photoskeeper; Owner: postgres
--

ALTER TABLE ONLY photos
    ADD CONSTRAINT photos_pkey PRIMARY KEY (id);


--
-- TOC entry 2699 (class 2606 OID 16467)
-- Name: tokens tokens_pkey; Type: CONSTRAINT; Schema: photoskeeper; Owner: postgres
--

ALTER TABLE ONLY tokens
    ADD CONSTRAINT tokens_pkey PRIMARY KEY (id);


--
-- TOC entry 2693 (class 2606 OID 16411)
-- Name: user user_pkey; Type: CONSTRAINT; Schema: photoskeeper; Owner: postgres
--

ALTER TABLE ONLY "user"
    ADD CONSTRAINT user_pkey PRIMARY KEY (id);


--
-- TOC entry 2695 (class 2606 OID 16413)
-- Name: user user_unique_constr; Type: CONSTRAINT; Schema: photoskeeper; Owner: postgres
--

ALTER TABLE ONLY "user"
    ADD CONSTRAINT user_unique_constr UNIQUE (email);


--
-- TOC entry 2700 (class 2606 OID 16455)
-- Name: photos phots_fk_user_id_constr; Type: FK CONSTRAINT; Schema: photoskeeper; Owner: postgres
--

ALTER TABLE ONLY photos
    ADD CONSTRAINT phots_fk_user_id_constr FOREIGN KEY (user_id) REFERENCES "user"(id);


--
-- TOC entry 2826 (class 0 OID 0)
-- Dependencies: 10
-- Name: photoskeeper; Type: ACL; Schema: -; Owner: postgres
--

GRANT ALL ON SCHEMA photoskeeper TO photoskeeperuser;


--
-- TOC entry 2827 (class 0 OID 0)
-- Dependencies: 200
-- Name: photos; Type: ACL; Schema: photoskeeper; Owner: postgres
--

GRANT ALL ON TABLE photos TO photoskeeperuser;


--
-- TOC entry 2828 (class 0 OID 0)
-- Dependencies: 201
-- Name: tokens; Type: ACL; Schema: photoskeeper; Owner: postgres
--

GRANT ALL ON TABLE tokens TO photoskeeperuser;


--
-- TOC entry 2829 (class 0 OID 0)
-- Dependencies: 199
-- Name: user; Type: ACL; Schema: photoskeeper; Owner: postgres
--

GRANT ALL ON TABLE "user" TO photoskeeperuser;


-- Completed on 2018-01-21 22:01:57

--
-- PostgreSQL database dump complete
--

