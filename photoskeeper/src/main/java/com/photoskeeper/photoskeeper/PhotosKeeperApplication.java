package com.photoskeeper.photoskeeper;

import com.photoskeeper.photoskeeper.config.PhotosKeeperConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;

@ComponentScan(basePackages = {"com.photoskeeper.photoskeeper"})
@EnableJpaRepositories
@EnableScheduling
@EnableAutoConfiguration
@Import({PhotosKeeperConfiguration.class})
public class PhotosKeeperApplication {

	public static void main(String[] args) {
		SpringApplication.run(PhotosKeeperApplication.class, args);
	}
}
