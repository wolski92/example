package com.photoskeeper.photoskeeper.controler;

import com.photoskeeper.photoskeeper.model.dto.PhotosDto;
import com.photoskeeper.photoskeeper.model.entity.common.Ordering;
import com.photoskeeper.photoskeeper.model.entity.photo.Photo;
import com.photoskeeper.photoskeeper.model.entity.photo.PhotoSortBy;
import com.photoskeeper.photoskeeper.service.PhotosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import static com.photoskeeper.photoskeeper.config.Constants.AUTH_HEADER;

/**
 * Created by Kuba on 19.01.2018.
 */

@RestController
@RequestMapping(value = "api/file")
@CrossOrigin
public class PhotoController {


    @Autowired
    PhotosService photosService;

    @RequestMapping(value = "", method = RequestMethod.POST)
    @ResponseBody
    public Photo uploadFile(@RequestHeader(value = "AUTH_HEADER") String token, @RequestParam(value = "file") MultipartFile multipartFile) {
        return photosService.save(token, multipartFile);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET,produces = MediaType.IMAGE_JPEG_VALUE)
    public byte[] getPhotoById(@PathVariable(value = "id") String id) {
        return photosService.getPhotoById(id).getOriginalFile();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity deletePhotoById(@PathVariable(value = "id") String id) {
        photosService.delete(id);
        return new ResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    public PhotosDto getPhotosForUser(@RequestHeader(value = AUTH_HEADER) String token,
                                      @RequestParam(required = false, defaultValue = "10") int numberOfResults,
                                      @RequestParam(value = "sortBy", required = false, defaultValue = "CREATION_DATE") PhotoSortBy photoSortBy,
                                      @RequestParam(required = false, defaultValue = "1") int pageNumber,
                                      @RequestParam(required = false, defaultValue = "ASC") Ordering ordering,
                                      @RequestParam(required = false) String searchPhrase) {

        return photosService.getPhotosForUser(token, photoSortBy, ordering, numberOfResults, pageNumber, searchPhrase);
    }
}
