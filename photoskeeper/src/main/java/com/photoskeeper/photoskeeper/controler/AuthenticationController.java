package com.photoskeeper.photoskeeper.controler;

import com.photoskeeper.photoskeeper.model.entity.Token;
import com.photoskeeper.photoskeeper.service.AuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

import static com.photoskeeper.photoskeeper.config.Constants.AUTH_HEADER;

/**
 * Created by Kuba on 19.01.2018.
 */

@RestController
@RequestMapping(value = "/auth")
public class AuthenticationController {



    @Autowired
    AuthenticationService authenticationService;

    @RequestMapping(value = "/signup",method = RequestMethod.POST)
    public ResponseEntity<?> signUp(@RequestParam String email, @RequestParam String password) {
        authenticationService.registerUser(email,password);
        return new ResponseEntity<String>(HttpStatus.OK);
    }



    @RequestMapping(value = "/login",method = RequestMethod.POST ,produces ="text/plain" )
    public ResponseEntity<String> authenticate(@RequestParam String email, @RequestParam String password) {
        if (email == null || password == null) {
            return new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
        }
        Token tokenDto =authenticationService.authenticateBasedOnEmailAndPassword(email,password);
        if (tokenDto != null) {
            HttpHeaders headers = new HttpHeaders();
            headers.add(AUTH_HEADER, tokenDto.getId());
            return new ResponseEntity<String>(tokenDto.getId(), headers, HttpStatus.CREATED);
        }

        return new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public ResponseEntity<String> logout(HttpServletRequest request) {
        String token = request.getHeader(AUTH_HEADER);
        if (token != null) {
            if(authenticationService.getUserByToken(token)!=null) {
                authenticationService.removeToken(token);
            }
        }

        return new ResponseEntity<String>(HttpStatus.OK);
    }
}
