package com.photoskeeper.photoskeeper.controler;

import com.photoskeeper.photoskeeper.model.entity.User;
import com.photoskeeper.photoskeeper.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import static com.photoskeeper.photoskeeper.config.Constants.AUTH_HEADER;

/**
 * Created by Kuba on 19.01.2018.
 */

@RestController
@RequestMapping(value = "/api/user")
public class UserController {

    @Autowired
    UserService userService;



    @RequestMapping(method = RequestMethod.PUT)
    public User changeEmail(@RequestHeader(value = AUTH_HEADER) String token, @RequestParam String email){
        return userService.update(token,email);
    }

}
