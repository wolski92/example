package com.photoskeeper.photoskeeper.scheduler;

import com.photoskeeper.photoskeeper.model.entity.Token;
import com.photoskeeper.photoskeeper.service.AuthenticationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * Created by Kuba on 19.01.2018.
 */
@Component
public class TokenScheduler {

    private static final Logger logger = LoggerFactory.getLogger(TokenScheduler.class);

    @Autowired
    AuthenticationService authenticationService;

    @Scheduled(fixedRate = 60000)
    public void removeExpiredTokens() {
        logger.info("Running token expiration job");
        for (Token token : authenticationService.tokensToRemove()) {
            logger.info("removing token for user with id: "+token.getUser().getId());
            authenticationService.removeToken(token.getId());
        }
    }


}
