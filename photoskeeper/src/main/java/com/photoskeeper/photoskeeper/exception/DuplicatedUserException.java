package com.photoskeeper.photoskeeper.exception;

/**
 * Created by Kuba on 20.01.2018.
 */
public class DuplicatedUserException extends RuntimeException {

    public DuplicatedUserException(String email){
        super("User with email "+email+" currently exist");
    }
}
