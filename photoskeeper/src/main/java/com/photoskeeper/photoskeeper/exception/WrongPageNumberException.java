package com.photoskeeper.photoskeeper.exception;

/**
 * Created by Kuba on 20.01.2018.
 */
public class WrongPageNumberException extends RuntimeException{

    public WrongPageNumberException(String userID){
        super("Wrong page number for user with id: " +userID);
    }
}
