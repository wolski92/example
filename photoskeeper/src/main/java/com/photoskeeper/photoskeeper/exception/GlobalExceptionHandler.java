package com.photoskeeper.photoskeeper.exception;

import org.springframework.http.HttpStatus;
import org.springframework.transaction.TransactionSystemException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.HandlerMapping;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolationException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Kuba on 20.01.2018.
 */

@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(DuplicatedUserException.class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    public @ResponseBody Map<String, Object> duplicatedUser() {
        final Map<String, Object> map = new HashMap<String, Object>();
        map.put("errorCode", 1100);
        map.put("errorMessage", "DuplicatedUserException");
        return map;
    }

    @ExceptionHandler(WrongPageNumberException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public @ResponseBody Map<String, Object> wrongPageNumber() {
        final Map<String, Object> map = new HashMap<String, Object>();
        map.put("errorCode", 1300);
        map.put("errorMessage", "WrongPageNumberException");
        return map;
    }
    @ExceptionHandler(WrongCredentialsException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public @ResponseBody Map<String, Object> wrongCredentials(HttpServletRequest request) {
        final Map<String, Object> map = new HashMap<String, Object>();
        map.put("errorCode", 1000);
        map.put("errorMessage", "WrongCredentialsException");
        request.removeAttribute(
                HandlerMapping.PRODUCIBLE_MEDIA_TYPES_ATTRIBUTE);
        return map;

    }

    @ExceptionHandler(PasswordToWeakException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public @ResponseBody Map<String, Object> passwordToWeakException() {
        final Map<String, Object> map = new HashMap<String, Object>();
        map.put("errorCode", 1200);
        map.put("errorMessage", "PasswordToWeakException");
        return map;
    }

    @ExceptionHandler(TransactionSystemException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public @ResponseBody Map<String, Object> constraintViolationException(TransactionSystemException e) {

        final Map<String, Object> map = new HashMap<String, Object>();
        if(e.getRootCause().getMessage().contains("propertyPath=email")){
            map.put("errorCode", 1750);
            map.put("errorMessage", "WrongEmailFormatException");}
        else {
            map.put("errorCode", 1700);
            map.put("errorMessage", "TransactionException");
        }
        return map;
    }



}
