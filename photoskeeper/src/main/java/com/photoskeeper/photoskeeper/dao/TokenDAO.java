package com.photoskeeper.photoskeeper.dao;

import com.photoskeeper.photoskeeper.model.entity.Token;
import com.photoskeeper.photoskeeper.model.entity.User;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;
import java.time.LocalDateTime;
import java.util.List;

import static com.photoskeeper.photoskeeper.config.Constants.TOKEN_DURATION_IN_MINUTES;

/**
 * Created by Kuba on 19.01.2018.
 */

@Repository
public class TokenDAO extends BaseDao<Token> {

    public List<Token> getTokensToRemove(){
        LocalDateTime limitTime = LocalDateTime.now().minusMinutes(TOKEN_DURATION_IN_MINUTES);
        TypedQuery<Token> query =
                entityManager.createQuery("SELECT t FROM Token t where t.expirationDate<:limitTime", Token.class);
        List<Token> results = query.setParameter("limitTime",limitTime).getResultList();
        return results;
    }

    public Token generateTokenForUser(final User user){
        return entityManager.merge(new Token(user));
    }

}
