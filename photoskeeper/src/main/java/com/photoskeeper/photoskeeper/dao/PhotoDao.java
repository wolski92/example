package com.photoskeeper.photoskeeper.dao;

import com.photoskeeper.photoskeeper.model.entity.common.Ordering;
import com.photoskeeper.photoskeeper.model.entity.photo.Photo;
import com.photoskeeper.photoskeeper.model.entity.photo.PhotoSortBy;
import com.photoskeeper.photoskeeper.model.entity.User;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kuba on 19.01.2018.
 */

@Repository
@Transactional
public class PhotoDao extends BaseDao<Photo> {


    public List<Photo> getPhotosForUser(User user, int numberOfResult, int page, Ordering ordering, PhotoSortBy sortBy, String searchPhrase) {

        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Photo> criteriaQuery = cb.createQuery(Photo.class);
        Root<Photo> photoRoot = criteriaQuery.from(Photo.class);
        criteriaQuery.where(cb.equal(photoRoot.get("user"), user)).orderBy();
        List<Order> orderList = new ArrayList();

        if (Ordering.DESC.equals(ordering)) {
            orderList.add(cb.desc(photoRoot.get(sortBy.getQueryString())));
        } else {
            orderList.add(cb.asc(photoRoot.get(sortBy.getQueryString())));
        }
        criteriaQuery.orderBy(orderList);
        if(StringUtils.hasText(searchPhrase)) {
            criteriaQuery.where(cb.like(photoRoot.get("photoName"),"%"+searchPhrase+"%" ));
        }
        Query query = entityManager.createQuery(criteriaQuery);
        query.setFirstResult((page - 1) * numberOfResult);
        query.setMaxResults(numberOfResult);
        return query.getResultList();
    }


    public int getNumberOfPhotosForUserWithSearchPhrase(User user, String searchPhrase){
        if(!StringUtils.hasText(searchPhrase)){
            searchPhrase="";
        }
        TypedQuery<Photo> query =
                entityManager.createQuery("SELECT p FROM Photo p where p.user = :user and p.photoName like :searchPhrase", Photo.class);
        query.setParameter("user",user);
        query.setParameter("searchPhrase","%"+searchPhrase+"%");
        return query.getResultList().size();
    }


}
