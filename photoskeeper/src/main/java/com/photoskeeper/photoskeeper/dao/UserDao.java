package com.photoskeeper.photoskeeper.dao;


import com.photoskeeper.photoskeeper.model.entity.User;
import org.slf4j.Logger;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

/**
 * Created by Kuba on 19.01.2018.
 */

@Repository
@Transactional
public class UserDao extends BaseDao<User>{

    private final static Logger logger = org.slf4j.LoggerFactory.getLogger(UserDao.class);

    public User getByEmail(final String email) {
        TypedQuery<User> query =
                entityManager.createQuery("SELECT u FROM User u where u.email = :userEmail", User.class);
        try {
          return   query.setParameter("userEmail",email).getSingleResult();
        }catch (NoResultException e){
            logger.info("User for email: "+email+" not found");
            return null;
        }
    }

}
