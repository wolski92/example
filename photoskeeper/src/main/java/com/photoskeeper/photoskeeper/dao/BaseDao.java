package com.photoskeeper.photoskeeper.dao;

import com.photoskeeper.photoskeeper.model.entity.common.BaseModel;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.MappedSuperclass;
import javax.validation.ConstraintViolationException;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

/**
 * Created by Kuba on 19.01.2018.
 */

@MappedSuperclass
public abstract class BaseDao<T extends BaseModel> {

    @Autowired
    EntityManager entityManager;

    protected final Class<T> type;

    public BaseDao() {
        final Type t = getClass().getGenericSuperclass();
        final ParameterizedType pt = (ParameterizedType) t;
        this.type = ((Class<T>) pt.getActualTypeArguments()[0]);
    }

    public T findById(final String id){
        return entityManager.find(type,id);
    }

    public T saveOrUpdate(final T object){
        return entityManager.merge(object);
    }

    public void removeById(final String id){
        T object = entityManager.find(type,id);
        entityManager.remove(object);
    }

}
