package com.photoskeeper.photoskeeper.model.entity.photo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.photoskeeper.photoskeeper.config.Constants;
import com.photoskeeper.photoskeeper.model.entity.common.BaseModel;
import com.photoskeeper.photoskeeper.model.entity.User;
import com.photoskeeper.photoskeeper.model.entity.converters.LocalDateTimeConverter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * Created by Kuba on 19.01.2018.
 */


@Entity
@Table(name="photos", schema = Constants.SCHEMA)
public class Photo extends BaseModel {

    public Photo() {
    }

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @Column(name = "file_name")
    String photoName;

    @Column(name = "original_file")
    @Basic(fetch = FetchType.LAZY)
    @JsonIgnore
    private byte[] originalFile;

    private byte[] thumbnail;

    @Column(name = "creation_date")
    @Convert(converter = LocalDateTimeConverter.class)
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime creationDate;

    public Photo(User user, String photoName, byte[] originalFile) {
        this.user = user;
        this.photoName =photoName;
        this.originalFile = originalFile;
        this.creationDate = LocalDateTime.now();
    }

    public String getPhotoName() {
        return photoName;
    }

    public User getUser() {
        return user;
    }

    public byte[] getOriginalFile() {
        return originalFile;
    }

    public byte[] getThumbnail() {
        return thumbnail;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public void setThumbnail(byte[] thumbnail) {
        this.thumbnail = thumbnail;
    }
}
