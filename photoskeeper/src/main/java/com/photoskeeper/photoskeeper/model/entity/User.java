package com.photoskeeper.photoskeeper.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.photoskeeper.photoskeeper.config.Constants;
import com.photoskeeper.photoskeeper.model.entity.common.BaseModel;
import org.hibernate.validator.constraints.Email;

import javax.persistence.*;

/**
 * Created by Kuba on 19.01.2018.
 */

@Entity
@Table(name="user", schema = Constants.SCHEMA)
public class User extends BaseModel{

    @Email
    private String email;

    @JsonIgnore
    private String password;

    public User() {
    }

    public User(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }
}
