package com.photoskeeper.photoskeeper.model.entity.photo;

/**
 * Created by Kuba on 20.01.2018.
 */
public enum PhotoSortBy {
    FILE_NAME("photoName"), CREATION_DATE("creationDate");

    private final String queryString;

    PhotoSortBy(String queryString) {
        this.queryString = queryString;
    }

    public String getQueryString() {
        return queryString;
    }

}
