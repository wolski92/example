package com.photoskeeper.photoskeeper.model.dto;

import com.photoskeeper.photoskeeper.model.entity.photo.Photo;

import java.util.List;

/**
 * Created by Kuba on 20.01.2018.
 */
public class PhotosDto {

    private List<Photo> photoList;

    private int totalNumberOfResults;

    private int numberOfPages;

    private int elementsPerPage;

    private int currentPage;

    public PhotosDto(List<Photo> photoList, int totalNumberOfResults, int numberOfPages, int elementsPerPage,int currentPage) {
        this.photoList = photoList;
        this.totalNumberOfResults = totalNumberOfResults;
        this.numberOfPages = numberOfPages;
        this.elementsPerPage =elementsPerPage;
        this.currentPage=currentPage;
    }

    public List<Photo> getPhotoList() {
        return photoList;
    }


    public int getTotalNumberOfResults() {
        return totalNumberOfResults;
    }


    public int getNumberOfPages() {
        return numberOfPages;
    }

    public int getElementsPerPage() {
        return elementsPerPage;
    }

    public int getCurrentPage() {
        return currentPage;
    }
}
