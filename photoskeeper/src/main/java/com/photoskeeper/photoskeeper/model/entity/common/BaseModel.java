package com.photoskeeper.photoskeeper.model.entity.common;

import org.springframework.beans.factory.annotation.Value;

import javax.persistence.*;
import java.util.UUID;

/**
 * Created by Kuba on 19.01.2018.
 */
@MappedSuperclass
public abstract class BaseModel {


    @Id
    private String id;

    public BaseModel() {
        if (this.id==null || this.id.equals("")){
            this.id= UUID.randomUUID().toString();
        }
    }

    public String getId() {
        return id;
    }
}
