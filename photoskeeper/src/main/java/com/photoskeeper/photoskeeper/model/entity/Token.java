package com.photoskeeper.photoskeeper.model.entity;

import com.photoskeeper.photoskeeper.config.Constants;
import com.photoskeeper.photoskeeper.model.entity.common.BaseModel;
import com.photoskeeper.photoskeeper.model.entity.converters.LocalDateTimeConverter;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * Created by Kuba on 19.01.2018.
 */
@Entity
@Table(name="tokens", schema = Constants.SCHEMA)
public class Token extends BaseModel {

    public Token() {
    }

    public Token(User user) {
        this.user = user;
        this.creationDate = LocalDateTime.now();
        this.expirationDate = LocalDateTime.now();
    }

    @ManyToOne(targetEntity = User.class, fetch = FetchType.EAGER)
    @JoinColumn(name = "id_user")
    private User user;

    @Column(name = "creation_date")
    @Convert(converter = LocalDateTimeConverter.class)
    private LocalDateTime creationDate;

    @Column(name = "expiration_date")
    @Convert(converter = LocalDateTimeConverter.class)
    private LocalDateTime expirationDate;


    public void updateExpirationDate(){
        this.expirationDate = LocalDateTime.now();
    }

    public User getUser() {
        return user;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public LocalDateTime getExpirationDate() {
        return expirationDate;
    }
}
