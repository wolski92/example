package com.photoskeeper.photoskeeper.service;

import com.photoskeeper.photoskeeper.dao.UserDao;
import com.photoskeeper.photoskeeper.model.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Kuba on 19.01.2018.
 */

@Service
@Transactional
public class UserService {

    @Autowired
    UserDao dao;

    @Autowired
    AuthenticationService authenticationService;

    public User save(final String email,final String password){
        return dao.saveOrUpdate(new User(email,password));
    }

    public User update(String token,String email){
        User user = authenticationService.getUserByToken(token);
        user.setEmail(email);
        return dao.saveOrUpdate(user);
    }

    public User getByEmail(final String email){
       return dao.getByEmail(email);
    }

}
