package com.photoskeeper.photoskeeper.service;

import com.photoskeeper.photoskeeper.dao.PhotoDao;
import com.photoskeeper.photoskeeper.exception.WrongPageNumberException;
import com.photoskeeper.photoskeeper.model.dto.PhotosDto;
import com.photoskeeper.photoskeeper.model.entity.common.Ordering;
import com.photoskeeper.photoskeeper.model.entity.photo.Photo;
import com.photoskeeper.photoskeeper.model.entity.photo.PhotoSortBy;
import com.photoskeeper.photoskeeper.model.entity.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.datatransfer.MimeTypeParseException;
import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.file.Paths;
import java.util.Base64;
import java.util.List;

/**
 * Created by Kuba on 19.01.2018.
 */

@Service
@Transactional
public class PhotosService {

    private final static Logger logger = LoggerFactory.getLogger(PhotosService.class);

    @Autowired
    PhotoDao photoDao;

    @Autowired
    AuthenticationService authenticationService;

    public Photo save(final String token, final MultipartFile file) {
        User user = authenticationService.getUserByToken(token);
        String fileName = Paths.get(file.getOriginalFilename()).getFileName().toString();
        Photo photo = null;
        try {
            photo = new Photo(user, fileName,  Base64.getEncoder().encode(file.getBytes()));
            photo.setThumbnail( Base64.getEncoder().encode(createThumbnail(file.getBytes())));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return photoDao.saveOrUpdate(photo);
    }

    public void delete(String photoId) {
        logger.info("Removing photo with id: " + photoId);
        photoDao.removeById(photoId);
    }

    public Photo getPhotoById(final String id) {
        return photoDao.findById(id);
    }

    public PhotosDto getPhotosForUser(String token, PhotoSortBy sortBy, Ordering sortAsc, int numberOfResult, int page, String searchPhrase) {
        User user = authenticationService.getUserByToken(token);
        int numberOfTotalResults = photoDao.getNumberOfPhotosForUserWithSearchPhrase(user, searchPhrase);
        int numberOfPages = numberOfTotalResults / numberOfResult + (numberOfTotalResults % numberOfResult == 0 ? 0 : 1);
        if (numberOfPages == 0) numberOfPages = 1;
        if (numberOfPages < page || page == 0) {
            logger.error("Wrong page number for user: " + user.getId());
            throw new WrongPageNumberException(user.getId());
        }
        List<Photo> photos = photoDao.getPhotosForUser(user, numberOfResult, page, sortAsc, sortBy, searchPhrase);
        return new PhotosDto(photos, numberOfTotalResults, numberOfPages, numberOfResult, page);
    }

    private byte[] createThumbnail(byte[] originalFile) throws IOException {
        InputStream in = new ByteArrayInputStream(originalFile);
        BufferedImage bufferedImage = ImageIO.read(in);
        BufferedImage img = new BufferedImage(50, 50, BufferedImage.TYPE_INT_RGB);
        img.createGraphics().drawImage(bufferedImage.getScaledInstance(100, 100, Image.SCALE_SMOOTH), 0, 0, null);

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(byteArrayOutputStream);
        ImageIO.write(img, "jpg", bufferedOutputStream);

        bufferedOutputStream.flush();
        byteArrayOutputStream.flush();
        byte[] newFile = byteArrayOutputStream.toByteArray();
        byteArrayOutputStream.close();
        bufferedOutputStream.close();
        return newFile;

    }


}
