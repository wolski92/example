package com.photoskeeper.photoskeeper.service;

import com.photoskeeper.photoskeeper.dao.TokenDAO;
import com.photoskeeper.photoskeeper.exception.DuplicatedUserException;
import com.photoskeeper.photoskeeper.exception.PasswordToWeakException;
import com.photoskeeper.photoskeeper.exception.WrongCredentialsException;
import com.photoskeeper.photoskeeper.model.entity.Token;
import com.photoskeeper.photoskeeper.model.entity.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Kuba on 19.01.2018.
 */
@Service
@Transactional
public class AuthenticationService {

    private final static Logger logger = LoggerFactory.getLogger(AuthenticationService.class);

    @Autowired
    UserService userService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    TokenDAO tokenDAO;

    public List<Token> tokensToRemove() {
        return tokenDAO.getTokensToRemove();
    }

    public Token authenticateBasedOnEmailAndPassword(final String email,final String password) {
        User user = userService.getByEmail(email);
        if (user != null && passwordEncoder.matches(password,user.getPassword())) {
            return tokenDAO.generateTokenForUser(user);
        }
        throw new WrongCredentialsException();
    }

    public void removeToken(String tokenID){
        tokenDAO.removeById(tokenID);
    }

    public void registerUser(final String email,final String password) {
        User user = userService.getByEmail(email);
        if (user != null) {
            throw new DuplicatedUserException(email);
        }
        if(!passwordValidator(password)){
            throw new PasswordToWeakException();
        }
        userService.save(email, passwordEncoder.encode(password));
    }

    public User getUserByToken(String token){
        Token tokendB = tokenDAO.findById(token);
        return tokendB!= null ? tokendB.getUser() : null;
    }

    public boolean isTokenValid(String token){
        return tokenDAO.findById(token)!=null? true:false;
    }


    private boolean passwordValidator(String password){
        String regex = "(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&])(?=\\S+$).{8,20}";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(password);
        return  matcher.matches();
    }

    public void refreshToken(String tokenId){
        Token token = tokenDAO.findById(tokenId);
        if(token !=null){
            logger.info("Token refreshed for user: " + token.getUser().getId());
            token.updateExpirationDate();
            tokenDAO.saveOrUpdate(token);
        }
    }
}
