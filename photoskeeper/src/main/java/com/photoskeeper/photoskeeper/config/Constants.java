package com.photoskeeper.photoskeeper.config;

/**
 * Created by Kuba on 19.01.2018.
 */
public class Constants {
    public final static String AUTH_HEADER="auth_header";
    public final static int TOKEN_DURATION_IN_MINUTES=60;
    public final static String SCHEMA = "photoskeeper";

}
