package com.photoskeeper.photoskeeper.security;

import com.photoskeeper.photoskeeper.exception.UnauthorizedException;
import com.photoskeeper.photoskeeper.service.AuthenticationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import static com.photoskeeper.photoskeeper.config.Constants.AUTH_HEADER;

/**
 * Created by Kuba on 19.01.2018.
 */
@Component
public class AuthenticationFilter extends GenericFilterBean {

    private final static Logger logger = LoggerFactory.getLogger(AuthenticationFilter.class);

    private RequestMatcher ignoredRequestsMatcher = new AntPathRequestMatcher("/auth/**");


    AuthenticationService authenticationService;

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) servletRequest;
        authenticationService = getAuthenticationService(servletRequest);
        Map<String, String> headerMaps = getHeadersInfo(httpRequest);
        String token = headerMaps.get(AUTH_HEADER);

        if (!httpRequest.getMethod().equals("OPTIONS") &&!ignoredRequestsMatcher.matches(httpRequest) && (token == null || !authenticationService.isTokenValid(token))) {
            if(token == null )
                logger.error("User token is null");
            else
                logger.error("User is not authorized. User token: " + token);
            ((HttpServletResponse) servletResponse).sendError(HttpServletResponse.SC_UNAUTHORIZED, "The token is not valid.");
        } else {
            if(token != null ) authenticationService.refreshToken(token);
            filterChain.doFilter(servletRequest, servletResponse);
        }
    }

    private Map<String, String> getHeadersInfo(HttpServletRequest request) {

        Map<String, String> map = new HashMap<String, String>();

        Enumeration headerNames = request.getHeaderNames();
        while (headerNames.hasMoreElements()) {
            String key = (String) headerNames.nextElement();
            String value = request.getHeader(key);
            map.put(key, value);
        }

        return map;
    }

    public AuthenticationService getAuthenticationService(ServletRequest request) {
        ServletContext servletContext = request.getServletContext();
        WebApplicationContext webApplicationContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
        return webApplicationContext.getBean(AuthenticationService.class);
    }
}
