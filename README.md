Back-end 
create schema for database
default database = postgres
default schema name= photoskeeper 
create user and add privileges to schema 
user/password: photoskeeperuser/photoskeeperuser

Run init sql.

Now you can run spring boot application with embedded tomcat on port 8080

---------------------------------------------------------------

Client-app

Run with http-server  on port 7777
(https://www.npmjs.com/package/http-server)

----------------------------------------------------------------

Current configuration assumed that both application are run on localhost on ports indicted above. 

Unfortunately I forgot to move the configuration of allowed origins from code to file so you can change app-client address in file 
photoskeeper / src / main / java / com / photoskeeper / photoskeeper / config / SecurityConfig.java

Configuration of client app can be changed in 
example / photoskepper-client / app / constants.js